<?php
/*
Plugin Name: Goyette Mechanical Tweaks
Plugin URI: http://www.kadimi.com/en/quote
Description: Miscellaneous enhancements
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

/**
 * Prevent words from breaking randomly
 */
add_action('wp_footer', 'fix_breaking_words');
function fix_breaking_words() {
	?><style>
		h1,h2,h3,h4,h5,h6,p,pre,.h1,.h2,.h3,.h4,.h5,.h6 {
				word-break: normal;
			-ms-word-break: normal;
					hyphens: auto;
			   -moz-hyphens: auto;
			-webkit-hyphens: auto;
			text-align: justify;
		}
	</style><?php 	
}
